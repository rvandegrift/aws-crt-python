Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: Amazon.com, Inc. or its affiliates.
License: Apache-2.0

Files: NOTICE
Copyright: 2018, Amazon.com, Inc. or its affiliates.
License: Apache-2.0

Files: debian/*
Copyright: 2021, Ross Vandegrift <rvandegrift@debian.org>
License: Apache-2.0

Files: docs/*
Copyright: 2020, Amazon Web Services, Inc.
License: Apache-2.0

Files: docs/.buildinfo
  docs/searchindex.js
Copyright: Amazon.com, Inc. or its affiliates.
License: Apache-2.0

Files: docs/_sources/*
Copyright: Amazon.com, Inc. or its affiliates.
License: Apache-2.0

Files: docs/_static/*
Copyright: Amazon.com, Inc. or its affiliates.
License: Apache-2.0

Files: docs/_static/basic.css
  docs/_static/doctools.js
  docs/_static/language_data.js
  docs/_static/searchtools.js
Copyright: Copyright 2007-2021,the Sphinx team, see AUTHORS.
License: Apache-2.0

Files: docs/_static/bizstyle.css
Copyright: Copyright 2011-2014,Sphinx team, see AUTHORS.
License: Apache-2.0

Files: docs/_static/bizstyle.js
Copyright: Copyright 2012-2014,Sphinx team, see AUTHORS.
License: Apache-2.0

Files: docs/_static/jquery-3.5.1.js
Copyright: JS Foundation and other contributors
License: Expat

Files: docs/_static/jquery.js
Copyright: JS Foundation and other contributors | jquery.org/license
License: Apache-2.0

Files: docs/_static/underscore-1.12.0.js
  docs/_static/underscore.js
Copyright: 2009-2020, Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
License: Expat

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
